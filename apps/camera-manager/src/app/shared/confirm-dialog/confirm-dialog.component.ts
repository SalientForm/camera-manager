import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import {
  CameraAssignment,
  CameraAssignmentFactory
} from '../../entities/camera/camera-assignment/camera-assignment.model';
import * as CameraAssignmentActions from '../../entities/camera/camera-assignment/camera-assignment.actions';

@Component({
  selector: 'cm-confirm-dialog',
  templateUrl: './confirm-dialog.component.html',
  styleUrls: ['./confirm-dialog.component.scss']
})
export class ConfirmDialogComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<ConfirmDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit(): void {
  }

  onClickConfirm(): void {
    this.dialogRef.close(true);
  }

  onClickDeny(): void {
    this.dialogRef.close(false);
  }

}
