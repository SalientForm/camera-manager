import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConfirmDialogComponent } from './confirm-dialog/confirm-dialog.component';
import { MaterialModule } from './material/material.module';
import { MatDialog } from '@angular/material/dialog';
import { CreateCameraAssignmentComponent } from '../views/create-camera-assignment/create-camera-assignment.component';

@NgModule({
  declarations: [ConfirmDialogComponent],
  imports: [
    CommonModule,
    MaterialModule
  ],
  exports: [
    MaterialModule
  ],
  providers: [
    MatDialog
  ],
  entryComponents: [
    ConfirmDialogComponent
  ]
})
export class SharedModule { }
