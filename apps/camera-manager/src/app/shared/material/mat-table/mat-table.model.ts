export interface ColumnConfig {
  name: string;
  label: string;
  getValue?: (unknown) => string;
}

export interface ColumnsConfig {
  [name: string]: ColumnConfig;
}

export interface TableRowAction {
  label: string;
  action: string;
  icon: string;
}

export interface TableRow {
  rowId?: string | number | null;
}

export interface ColumnConstraint {
  property: string;
  conditional: (value: any) => boolean;
}

export interface TableRowFilter {
  searchTerms: string[];
  conditionals: ColumnConstraint[];
}
