import { Component, OnInit, OnDestroy, ChangeDetectionStrategy, HostBinding, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ColumnsConfig } from '../../shared/material/mat-table/mat-table.model';
import { Camera } from '../../entities/camera/camera.model';
import { VehicleFacade } from '../../entities/vehicle/vehicle.facade';
import { Subscription } from 'rxjs';
import { HydratedVehicle } from '../../entities/vehicle/vehicle.model';
import { Dictionary } from '@ngrx/entity';
import { CameraFacade } from '../../entities/camera/camera.facade';

export interface VehicleDataRow {
  id: number;
  name: string;
  type: string;
  cameras: Array<Camera>;
}

@Component({
  selector: 'cm-vehicles',
  templateUrl: './vehicles.component.html',
  styleUrls: ['./vehicles.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class VehiclesComponent implements OnInit, OnDestroy {

  subscription: Subscription;
  vehicles: Array<HydratedVehicle> = [];

  @HostBinding('class') class = 'page';
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  columnsConfig: ColumnsConfig = {
    id: { name: 'id', label: 'Id', getValue: (row) => row.id },
    name: { name: 'name', label: 'Name', getValue: (row) => row.name },
    type: { name: 'type', label: 'Type', getValue: (row) => row.type },
    cameras: { name: 'cameras', label: 'Cameras', getValue: (row) => this.getCamerasText(row.cameras) }
  };

  displayedColumns: string[] = ['id', 'name', 'type', 'cameras'];
  tableDataSource = new MatTableDataSource();

  constructor(
    private vehicleFacade: VehicleFacade
  ) {
  }

  ngOnInit(): void {
    this.tableDataSource.sort = this.sort;
    this.subscription =
      this.vehicleFacade.selectHydratedVehicles$.subscribe((vehicles) => {
        this.vehicles = vehicles;
        this.updateTableData();
      });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  getCamerasText(cameras: Array<Camera> = []): string {
    let text = '';
    cameras.forEach((c, i) => {
      if (i > 0) {
        text = text + ', ' + c.DeviceNo;
      } else {
        text = text + c.DeviceNo;
      }
    });
    return (text) ? text : '(none)';
  }

  updateTableData() {
    const vehicleDataRows = [];
    this.vehicles.forEach((v) => {
      const vehicleDataRow: VehicleDataRow = {
        id: v.Id,
        name: v.Name,
        type: v.Type,
        cameras: v.cameras
      };
      vehicleDataRows.push(vehicleDataRow);
    });
    this.tableDataSource.data = vehicleDataRows;
  }

}

