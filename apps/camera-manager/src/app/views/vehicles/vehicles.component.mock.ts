import { VehicleTypes } from '../../entities/vehicle/vehicle.model';
import { VehicleDataRow } from './vehicles.component';
import { camera_seedData } from '../../entities/camera/camera.mock';

export const DUMMY_VEHICLE_DATA: VehicleDataRow[] = [
  { id: 1, type: VehicleTypes.REEFER, cameras: [camera_seedData[0], camera_seedData[1]] },
  { id: 2, type: VehicleTypes.CONTAINER, cameras: [camera_seedData[2]] },
  { id: 3, type: VehicleTypes.BOX, cameras: [] }
];
