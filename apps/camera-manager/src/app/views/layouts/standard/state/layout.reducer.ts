
import { LayoutActions, LayoutActionTypes } from './layout.actions';

export const layoutFeatureKey = 'layout';

export interface State {

}

export const initialState: State = {

};

export function reducer(state = initialState, action: LayoutActions): State {
  switch (action.type) {

    case LayoutActionTypes.LoadLayouts:
      return state;

    default:
      return state;
  }
}
