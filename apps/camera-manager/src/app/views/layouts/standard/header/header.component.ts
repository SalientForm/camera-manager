import { Component, OnInit, OnDestroy } from '@angular/core';
import { UserFacade } from '../../../../entities/user/user.facade';
import { combineLatest, Subscription } from 'rxjs';
import { User } from '../../../../entities/user/user.model';

@Component({
  selector: 'cm-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, OnDestroy {

  private subscription: Subscription;
  private localUser: User;
  private users: User[];

  constructor(private userFacade:UserFacade) { }

  ngOnInit(): void {
    this.subscription = combineLatest([
      this.userFacade.selectAll$,
      this.userFacade.selectLocalUser$
    ]).subscribe(([users, localUser]) => {
      this.users = users;
      this.localUser = localUser;
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
