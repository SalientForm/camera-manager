import { Component, OnInit } from '@angular/core';

interface MenuItem {
  link: string;
  label: string;
  icon: string;
}

@Component({
  selector: 'cm-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

  menuItems: Array<MenuItem>;

  constructor() { }

  ngOnInit(): void {
    this.menuItems = [
      { link: 'summary', label: 'Summary', icon: 'dashboard'},
      { link: 'vehicles', label: 'Vehicles', icon: 'local_shipping'},
      { link: 'cameras', label: 'Cameras', icon: 'camera' },
      { link: 'assignments', label: 'Assignments', icon: 'assignment'}
    ];
  }

}
