import { CameraAssignmentTableRow } from './camera-assignments.component';
import { camera_seedData } from '../../entities/camera/camera.mock';
import { vehicle_seedData } from '../../entities/vehicle/vehicle.mock';
import { user_seedData } from '../../entities/user/user.mock';

export const DUMMY_CAMERA_ASSIGNMENTS_DATA:Array<CameraAssignmentTableRow> = [
  {
    id: 1,
    camera: camera_seedData[0],
    vehicle: vehicle_seedData[0],
    dateCreated: 1597077009380,
    deleted: false,
    assignedBy: user_seedData[1]
  },
  {
    id: 2,
    camera: camera_seedData[1],
    vehicle: vehicle_seedData[0],
    dateCreated: 1597077009380,
    deleted: false,
    assignedBy: user_seedData[1]
  },
  {
    id: 3,
    camera: camera_seedData[2],
    vehicle: vehicle_seedData[1],
    dateCreated: 1597077009380,
    deleted: false,
    assignedBy: user_seedData[2]
  }
];
