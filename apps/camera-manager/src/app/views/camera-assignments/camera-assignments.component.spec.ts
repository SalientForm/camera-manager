import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CameraAssignmentsComponent } from './camera-assignments.component';

describe('AssignmentsComponent', () => {
  let component: CameraAssignmentsComponent;
  let fixture: ComponentFixture<CameraAssignmentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CameraAssignmentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CameraAssignmentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
