import { Component, OnInit, HostBinding, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import {
  ColumnsConfig,
  TableRow,
  TableRowAction,
  TableRowFilter
} from '../../shared/material/mat-table/mat-table.model';
import { MatTableDataSource } from '@angular/material/table';
import { Camera } from '../../entities/camera/camera.model';
import { Vehicle } from '../../entities/vehicle/vehicle.model';
import { User } from '../../entities/user/user.model';
import { CameraFacade } from '../../entities/camera/camera.facade';
import {
  HydratedCameraAssignment
} from '../../entities/camera/camera-assignment/camera-assignment.model';
import { MatDialog } from '@angular/material/dialog';
import { CreateCameraAssignmentComponent } from '../create-camera-assignment/create-camera-assignment.component';
import * as CameraAssignmentActions from '../../entities/camera/camera-assignment/camera-assignment.actions';
import { combineLatest } from 'rxjs';
import { ConfirmDialogComponent } from '../../shared/confirm-dialog/confirm-dialog.component';
import { first } from 'rxjs/operators';

export interface CameraAssignmentTableRow extends TableRow {
  id: number;
  camera: Camera;
  vehicle: Vehicle;
  dateCreated: number;
  assignedBy: User;
  deleted: boolean;
}

export const CameraAssignmentRowActions = {
  DELETE: 'delete',
  EDIT: 'edit'
};

@Component({
  selector: 'cm-assignments',
  templateUrl: './camera-assignments.component.html',
  styleUrls: ['./camera-assignments.component.scss']
})
export class CameraAssignmentsComponent implements OnInit {

  subscription: any;
  hydratedCameraAssignments: HydratedCameraAssignment[];

  @HostBinding('class') class = 'page';
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  columnsConfig: ColumnsConfig = {
    id: { name: 'id', label: 'Id', getValue: (row) => row.id },
    camera: { name: 'camera', label: 'Camera', getValue: (row) => row.camera.DeviceNo },
    vehicle: { name: 'vehicle', label: 'Vehicle', getValue: (row) => row.vehicle.Name },
    deleted: { name: 'deleted', label: 'Active', getValue: (row) => (row.deleted) ? 'No' : 'Yes' },
    actions: { name: 'actions', label: 'Actions' }
  };

  actions: Array<TableRowAction> = [
    { label: 'Delete', action: CameraAssignmentRowActions.DELETE, icon: 'delete' }
  ];

  displayedColumns: string[] = ['id', 'camera', 'vehicle', 'actions'];
  tableDataSource = new MatTableDataSource();
  filter = '';
  private selectedCameraAssignmentId: number;

  private tableRowFilter:TableRowFilter = {
    searchTerms: [],
    conditionals: [
      {
        property: 'deleted',
        conditional: (value) => !value // if not deleted, show row
      }
    ]
  };

  constructor(
    private cameraFacade: CameraFacade,
    private matDialog: MatDialog
  ) {
  }

  ngOnInit(): void {
    this.subscription = combineLatest([
      this.cameraFacade.selectHydratedCameraAssignments$
    ]).subscribe(
      ([hydratedCameraAssignments]) => {
        this.hydratedCameraAssignments = hydratedCameraAssignments;
        this.updateTableData();
      }
    );
  }

  updateTableData() {
    const cameraAssignmentDataRows = [];
    this.hydratedCameraAssignments.forEach((hca) => {
      const cameraAssignmentDataRow: CameraAssignmentTableRow = {
        id: hca.Id,
        camera: hca.camera,
        vehicle: hca.vehicle,
        dateCreated: hca.DateCreated,
        assignedBy: hca.assignedBy,
        deleted: hca.Deleted
      };
      if (this.validateTableRow(cameraAssignmentDataRow, this.tableRowFilter)) cameraAssignmentDataRows.push(cameraAssignmentDataRow);
    });
    this.tableDataSource.data = cameraAssignmentDataRows;
  }

  // TODO: implement use of tableRowFilter, move to helper function in shared
  validateTableRow(row:CameraAssignmentTableRow, tableRowFilter: TableRowFilter):boolean {
    // future consideration: fuse.js
    let isValid = true;
    if (this.filter.length > 0) {
      isValid = false;
      const words: Array<string> = this.filter.split(' ');
      words.forEach((word) => {
        word = word.toLowerCase();
        if (row.camera.DeviceNo.toLowerCase().indexOf(word) !== -1) {
          isValid = true;
        } else if (row.vehicle.Name.toLowerCase().indexOf(word) !== -1) {
          isValid = true;
        }
      });
    }

    if(row.deleted) isValid = false;
    return isValid;
  }

  showCreateAssignmentDialog() {
    const dialogRef = this.matDialog.open(CreateCameraAssignmentComponent,
      {
        // height: '19rem',
        width: '36rem',
        data: {}
      }
    );
  }

  onClickToggleShowDeleted() {

  }

  getDeletedToggleText() {
    return 'Show Deleted';
  }

  updateFilter(e: any) {
    this.filter = e.target.value;
    this.updateTableData();
  }

  onDeleteAction(cameraAssignmentId: string) {

    this.selectedCameraAssignmentId = parseInt(cameraAssignmentId, 10);

    const cameraName = this.hydratedCameraAssignments[cameraAssignmentId].camera.DeviceNo;
    const vehicleName = this.hydratedCameraAssignments[cameraAssignmentId].vehicle.Name;

    const dialogRef = this.matDialog.open(ConfirmDialogComponent,
      {
        width: '36rem',
        data: {
          title: 'Confirm Delete',
          text: `Remove the assignment of camera <b>${cameraName}</b> to vehicle <b>${vehicleName}</b>?`
        }
      }
    );

    dialogRef.afterClosed().pipe(first()).subscribe(result => {
      if (result === true) {
        this.onConfirm();
      }
    });
  }

  onConfirm() {
    this.cameraFacade.dispatch(CameraAssignmentActions.deleteCameraAssignment({ id: this.selectedCameraAssignmentId }));
  }
}
