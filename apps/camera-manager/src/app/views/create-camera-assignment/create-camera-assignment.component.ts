import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { CameraFacade } from '../../entities/camera/camera.facade';
import { Camera } from '../../entities/camera/camera.model';
import { Vehicle } from '../../entities/vehicle/vehicle.model';
import {
  CameraAssignment,
  CameraAssignmentFactory
} from '../../entities/camera/camera-assignment/camera-assignment.model';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import * as CameraAssignmentActions from '../../entities/camera/camera-assignment/camera-assignment.actions';
import { Subscription } from 'rxjs';
import { combineLatest } from 'rxjs';
import { User } from '../../entities/user/user.model';
import { UserFacade } from '../../entities/user/user.facade';

@Component({
  selector: 'cm-create-camera-assignment',
  templateUrl: './create-camera-assignment.component.html',
  styleUrls: ['./create-camera-assignment.component.scss']
})
export class CreateCameraAssignmentComponent implements OnInit, OnDestroy {

  subscription: Subscription;
  filteredCameras: Camera[] = [];
  filteredVehicles: Vehicle[] = [];
  localUser: User;
  createCameraAssignmentGroup: FormGroup;

  FormProps = {
    vehicleId: 'vehicleId',
    cameraId: 'cameraId'
  };

  constructor(
    public dialogRef: MatDialogRef<CreateCameraAssignmentComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private cameraFacade: CameraFacade,
    private userFacade: UserFacade,
    private formBuilder: FormBuilder
  ) {
  }

  ngOnInit(): void {
    this.createCameraAssignmentGroup = this.formBuilder.group({
      [this.FormProps.vehicleId]: ['', { validators: [Validators.required] }],
      [this.FormProps.cameraId]: ['', { validators: [Validators.required] }]
    });

    this.subscription = combineLatest([
      this.cameraFacade.selectVehiclesWithoutCameraAssignment$,
      this.cameraFacade.selectAllUnassignedCameras$,
      this.userFacade.selectLocalUser$
    ]).subscribe(([v, c, u]) => {
      this.filteredVehicles = v;
      this.filteredCameras = c;
      this.localUser = u;
    });

  }

  onClickSubmit(): void {
    const cameraAssignment: CameraAssignment = CameraAssignmentFactory(
      {
        VehicleId: this.createCameraAssignmentGroup.get(this.FormProps.vehicleId).value,
        CameraId: this.createCameraAssignmentGroup.get(this.FormProps.cameraId).value,
        AssignedById: this.localUser.Id
      }
    );
    this.cameraFacade.dispatch(CameraAssignmentActions.addCameraAssignment({ cameraAssignment: cameraAssignment }));
    this.dialogRef.close();
  }

  isValidSelectionPossible() {
    return this.filteredCameras.length > 0 && this.filteredVehicles.length > 0;
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
