import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateCameraAssignmentComponent } from './create-camera-assignment.component';

describe('CreateCameraAssignmentComponent', () => {
  let component: CreateCameraAssignmentComponent;
  let fixture: ComponentFixture<CreateCameraAssignmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateCameraAssignmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateCameraAssignmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
