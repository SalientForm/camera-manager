import { CameraDataRow } from './cameras.component';
import { CameraLocations } from '../../entities/camera/camera.model';
import { vehicle_seedData } from '../../entities/vehicle/vehicle.mock';

export const DUMMY_CAMERA_DATA:Array<CameraDataRow> = [
  {id: 1, stockId: 'abc123', deviceNo: 'super-4-8-15-16-23-42', location: CameraLocations.INSTALLED_IN_VEHICLE, vehicle: vehicle_seedData[0]},
  {id: 2, stockId: 'xyz789', deviceNo: 'mystery-theater-cam-3000', location: CameraLocations.WAREHOUSE, vehicle: vehicle_seedData[0]},
  {id: 3, stockId: '1234abcd', deviceNo: 'buck-shot-2500', location: CameraLocations.INSTALLED_IN_VEHICLE, vehicle: vehicle_seedData[1]}
];
