import { Component, OnInit, OnDestroy, ChangeDetectionStrategy, HostBinding, ViewChild } from '@angular/core';
import { ColumnsConfig } from '../../shared/material/mat-table/mat-table.model';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Vehicle } from '../../entities/vehicle/vehicle.model';
import { CameraFacade } from '../../entities/camera/camera.facade';
import { CameraStatuses, HydratedCamera } from '../../entities/camera/camera.model';

export interface CameraDataRow {
  id: number;
  stockId: string;
  location: string;
  deviceNo: string;
  vehicle?: Vehicle;
}

export const CameraActions =  {
  EDIT: 'edit'
}

@Component({
  selector: 'cm-cameras',
  templateUrl: './cameras.component.html',
  styleUrls: ['./cameras.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CamerasComponent implements OnInit, OnDestroy {

  subscription: any;
  cameras: Array<HydratedCamera> = [];

  @HostBinding('class') class = 'page';
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  columnsConfig: ColumnsConfig = {
    id: { name: 'id', label: 'Id', getValue: (row) => row.id },
    stockId: { name: 'stockId', label: 'Stock Id', getValue: (row) => row.stockId },
    deviceNo: { name: 'deviceNo', label: 'DeviceNo', getValue: (row) => row.deviceNo },
    location: { name: 'location', label: 'Current Location', getValue: (row) => row.location },
    vehicle: { name: 'vehicle', label: 'Assigned Vehicle', getValue: (row) => (row.vehicle) ? row.vehicle.Name : CameraStatuses.UNASSIGNED }
  };

  displayedColumns: string[] = ['id', 'stockId', 'deviceNo', 'location', 'vehicle'];
  tableDataSource = new MatTableDataSource();

  constructor(
    private cameraFacade: CameraFacade
  ) {
  }

  ngOnInit(): void {
    // select table data
    this.subscription = this.cameraFacade.selectHydratedCameras$.subscribe((cameras) => {
      this.cameras = cameras;
      this.updateTableData();
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  updateTableData() {
    const cameraDataRows = [];
    this.cameras.forEach((c) => {
      const cameraDataRow: CameraDataRow = {
        id: c.Id,
        stockId: c.StockId,
        location: c.Location,
        deviceNo: c.DeviceNo,
        vehicle: c.vehicleAssignment
      };
      cameraDataRows.push(cameraDataRow);
    });
    this.tableDataSource.data = cameraDataRows;
  }

}
