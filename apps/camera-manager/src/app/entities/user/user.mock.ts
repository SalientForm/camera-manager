import { User } from './user.model';

export const user_seedData:Array<User> = [
  { Id: 1, FirstName: 'Tammie', LastName: "Kevorkian" },
  { Id: 2, FirstName: 'Barbara', LastName: "Ericsson" },
  { Id: 3, FirstName: 'Dunn', LastName: "Williams" }
];

