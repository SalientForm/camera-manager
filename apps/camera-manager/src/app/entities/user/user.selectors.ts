import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromUsers from './user.reducer';

export const selectUserState = createFeatureSelector<fromUsers.State>(fromUsers.userFeatureKey);

const { selectAll, selectEntities } = fromUsers.adapter.getSelectors();

export const selectAllUsers = createSelector(
  selectUserState,
  (state: fromUsers.State) => selectAll(state)
);

export const selectUserEntities = createSelector(
  selectUserState,
  (state: fromUsers.State) => selectEntities(state)
);

export const selectLocalUser = createSelector(
  selectUserState,
  selectUserEntities,
  (state: fromUsers.State, userEntities) => userEntities[state.localUserId]
);
