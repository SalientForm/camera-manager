import * as fromUser from './user.reducer';
import * as UserSelectors from './user.selectors';

describe('User Selectors', () => {
  it('should select the feature state', () => {
    const result = UserSelectors.selectUserState({
      [fromUser.userFeatureKey]: {}
    });

    expect(result).toEqual({});
  });
});
