import { Injectable } from '@angular/core';
import { Action, select, Store } from '@ngrx/store';
import * as fromUsers from '../user/user.reducer';
import * as UserSelectors from './user.selectors';

@Injectable({
  providedIn: 'root'
})
export class UserFacade {

  constructor(private store: Store<fromUsers.State>) {}

  selectAll$ = this.store.pipe(select(UserSelectors.selectAllUsers));
  selectEntities$ = this.store.pipe(select(UserSelectors.selectUserEntities));
  selectLocalUser$ = this.store.pipe(select(UserSelectors.selectLocalUser));

  dispatch(action: Action) {
    this.store.dispatch(action);
  }
}
