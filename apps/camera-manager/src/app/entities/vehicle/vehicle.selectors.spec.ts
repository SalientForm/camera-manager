import * as fromVehicle from './vehicle.reducer';
import { selectVehicleState } from './vehicle.selectors';

describe('Vehicle Selectors', () => {
  it('should select the feature state', () => {
    const result = selectVehicleState({
      [fromVehicle.vehicleFeatureKey]: {}
    });

    expect(result).toEqual({});
  });
});
