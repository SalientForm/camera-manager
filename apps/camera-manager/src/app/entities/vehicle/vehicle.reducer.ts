import { createReducer, on } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';
import { Vehicle } from './vehicle.model';
import * as VehicleActions from './vehicle.actions';

export const vehicleFeatureKey = 'vehicles';

export interface State extends EntityState<Vehicle> {
  // additional entities state properties
}

export const adapter: EntityAdapter<Vehicle> = createEntityAdapter<Vehicle>({
  selectId: (v => v.Id)
});

export const initialState: State = adapter.getInitialState({
  // additional entity state properties
});


export const reducer = createReducer(
  initialState,
  on(VehicleActions.addVehicle,
    (state, action) => adapter.addOne(action.vehicle, state)
  ),
  on(VehicleActions.upsertVehicle,
    (state, action) => adapter.upsertOne(action.vehicle, state)
  ),
  on(VehicleActions.addVehicles,
    (state, action) => adapter.addMany(action.vehicles, state)
  ),
  on(VehicleActions.upsertVehicles,
    (state, action) => adapter.upsertMany(action.vehicles, state)
  ),
  on(VehicleActions.updateVehicle,
    (state, action) => adapter.updateOne(action.vehicle, state)
  ),
  on(VehicleActions.updateVehicles,
    (state, action) => adapter.updateMany(action.vehicles, state)
  ),
  on(VehicleActions.deleteVehicle,
    (state, action) => adapter.removeOne(action.id, state)
  ),
  on(VehicleActions.deleteVehicles,
    (state, action) => adapter.removeMany(action.ids, state)
  ),
  on(VehicleActions.loadVehicles,
    (state, action) => adapter.setAll(action.vehicles, state)
  ),
  on(VehicleActions.clearVehicles,
    state => adapter.removeAll(state)
  ),
);


export const {
  selectIds,
  selectEntities,
  selectAll,
  selectTotal,
} = adapter.getSelectors();
