import { Injectable } from '@angular/core';
import { Action, select, Store } from '@ngrx/store';
import * as fromVehicles from '../vehicle/vehicle.reducer';
import * as VehicleSelectors from './vehicle.selectors';
import * as VehicleDomainSelectors from './vehicle.domain-selectors';

@Injectable({
  providedIn: 'root'
})
export class VehicleFacade {

  constructor(private store: Store<fromVehicles.State>) {}

  selectAll$ = this.store.pipe(select(VehicleSelectors.selectAllVehicles));
  selectEntities$ = this.store.pipe(select(VehicleSelectors.selectVehicleEntities));
  selectHydratedVehicles$ = this.store.pipe(select(VehicleDomainSelectors.selectHydratedVehicles));

  dispatch(action: Action) {
    this.store.dispatch(action);
  }
}
