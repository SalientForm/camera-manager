import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromVehicle from './vehicle.reducer';

export const selectVehicleState = createFeatureSelector<fromVehicle.State>(
  fromVehicle.vehicleFeatureKey
);

const { selectAll, selectEntities } = fromVehicle.adapter.getSelectors();

export const selectAllVehicles = createSelector(selectVehicleState, (state: fromVehicle.State) =>
  selectAll(state)
);

export const selectVehicleEntities = createSelector(
  selectVehicleState,
  (state: fromVehicle.State) => selectEntities(state)
);

