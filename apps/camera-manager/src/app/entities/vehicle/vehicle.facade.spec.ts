import { TestBed } from '@angular/core/testing';
import { VehicleFacade } from './vehicle.facade';

describe('VehicleFacade', () => {
  let service: VehicleFacade;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(VehicleFacade);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
