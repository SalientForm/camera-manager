import { Camera } from '../camera/camera.model';

export interface Vehicle {
  Id: number;
  Type: string;
  Name: string;
}

export interface HydratedVehicle extends Vehicle {
  cameras: Camera[];
}

export const VehicleTypes = {
  BOX: 'box',
  CAR_HAULER: 'car-hauler',
  CONTAINER: 'container',
  DRY_BULK: 'dry-bulk',
  FLATBED: 'flatbed',
  LOWBOY: 'lowboy',
  REEFER: 'reefer',
  TANKER: 'tanker'
}
