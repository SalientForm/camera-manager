import { Vehicle, VehicleTypes } from './vehicle.model';

export const vehicle_seedData:Array<Vehicle> = [
  {Id: 1, Type: VehicleTypes.REEFER, Name: 'Cool Boy Jones'},
  {Id: 2, Type: VehicleTypes.CONTAINER, Name: 'Mueller The Hauler'},
  {Id: 3, Type: VehicleTypes.BOX, Name: 'Just Move It Michelson'},
  {Id: 4, Type: VehicleTypes.LOWBOY, Name: 'Taking it slow Johnson'}
];
