import { createSelector } from '@ngrx/store';
import * as CameraDomainSelectors from '../camera/camera.domain-selectors';
import * as VehicleSelectors from './vehicle.selectors';
import { HydratedVehicle } from './vehicle.model';

export const selectHydratedVehicles = createSelector(
  VehicleSelectors.selectAllVehicles,
  CameraDomainSelectors.selectCamerasByVehicle,
  (vehicles, camerasByVehicle) => {
    const hydratedVehicles: HydratedVehicle[] = [];
    vehicles.forEach((v)=>{
      const hv:HydratedVehicle = {
        ...v,
        cameras: camerasByVehicle[v.Id]
      };
      hydratedVehicles.push(hv);
    });
    return hydratedVehicles;
  }
);
