import { createReducer, on } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';
import { Camera } from './camera.model';
import * as CameraActions from './camera.actions';

export const cameraFeatureKey = 'cameras';

export interface State extends EntityState<Camera> {
  // additional entities state properties
}

export const adapter: EntityAdapter<Camera> = createEntityAdapter<Camera>({
  selectId: (c => c.Id)
});

export const initialState: State = adapter.getInitialState({
  // additional entity state properties
});


export const reducer = createReducer(
  initialState,
  on(CameraActions.addCamera,
    (state, action) => adapter.addOne(action.camera, state)
  ),
  on(CameraActions.upsertCamera,
    (state, action) => adapter.upsertOne(action.camera, state)
  ),
  on(CameraActions.addCameras,
    (state, action) => adapter.addMany(action.cameras, state)
  ),
  on(CameraActions.upsertCameras,
    (state, action) => adapter.upsertMany(action.cameras, state)
  ),
  on(CameraActions.updateCamera,
    (state, action) => adapter.updateOne(action.camera, state)
  ),
  on(CameraActions.updateCameras,
    (state, action) => adapter.updateMany(action.cameras, state)
  ),
  on(CameraActions.deleteCamera,
    (state, action) => adapter.removeOne(action.id, state)
  ),
  on(CameraActions.deleteCameras,
    (state, action) => adapter.removeMany(action.ids, state)
  ),
  on(CameraActions.loadCameras,
    (state, action) => adapter.setAll(action.cameras, state)
  ),
  on(CameraActions.clearCameras,
    state => adapter.removeAll(state)
  )
);


export const {
  selectIds,
  selectEntities,
  selectAll,
  selectTotal
} = adapter.getSelectors();
