import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Observable } from 'rxjs';

import { CameraEffects } from './camera.effects';

describe('CameraEffects', () => {
  let actions$: Observable<any>;
  let effects: CameraEffects;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        CameraEffects,
        provideMockActions(() => actions$)
      ]
    });

    effects = TestBed.get<CameraEffects>(CameraEffects);
  });

  it('should be created', () => {
    expect(effects).toBeTruthy();
  });
});
