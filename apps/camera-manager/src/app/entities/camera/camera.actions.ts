import { createAction, props } from '@ngrx/store';
import { Update } from '@ngrx/entity';

import { Camera } from './camera.model';

export const loadCameras = createAction(
  '[Camera/API] Load Cameras',
  props<{ cameras: Camera[] }>()
);

export const loadCamerasSuccess = createAction(
  '[Camera/API] Load Cameras Success',
  props<{ cameras: Camera[] }>()
);

export const loadCamerasFailure = createAction(
  '[Camera/API] Load Cameras Failure',
  props<{ error: any }>()
);

export const addCamera = createAction(
  '[Camera/API] Add Camera',
  props<{ camera: Camera }>()
);

export const upsertCamera = createAction(
  '[Camera/API] Upsert Camera',
  props<{ camera: Camera }>()
);

export const addCameras = createAction(
  '[Camera/API] Add Cameras',
  props<{ cameras: Camera[] }>()
);

export const upsertCameras = createAction(
  '[Camera/API] Upsert Cameras',
  props<{ cameras: Camera[] }>()
);

export const updateCamera = createAction(
  '[Camera/API] Update Camera',
  props<{ camera: Update<Camera> }>()
);

export const updateCameras = createAction(
  '[Camera/API] Update Cameras',
  props<{ cameras: Update<Camera>[] }>()
);

export const deleteCamera = createAction(
  '[Camera/API] Delete Camera',
  props<{ id: string }>()
);

export const deleteCameras = createAction(
  '[Camera/API] Delete Cameras',
  props<{ ids: string[] }>()
);

export const clearCameras = createAction(
  '[Camera/API] Clear Cameras'
);
