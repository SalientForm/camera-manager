import { createSelector } from '@ngrx/store';
import * as CameraSelectors from './camera.selectors';
import * as VehicleSelectors from '../vehicle/vehicle.selectors';
import * as UserSelectors from '../user/user.selectors';
import * as CameraAssignmentSelectors from './camera-assignment/camera-assignment.selectors';
import { Dictionary } from '@ngrx/entity';
import { CameraAssignment, HydratedCameraAssignment } from './camera-assignment/camera-assignment.model';
import { Camera, HydratedCamera } from './camera.model';
import { Vehicle } from '../vehicle/vehicle.model';
import { selectAllCameraAssignments } from './camera-assignment/camera-assignment.selectors';

export const selectHydratedCameras = createSelector(
  CameraSelectors.selectAllCameras,
  VehicleSelectors.selectVehicleEntities,
  CameraAssignmentSelectors.selectAllCameraAssignments,
  (cameras, vehicleEntities, cameraAssignments) => {

    const cameraAssignmentsByCamera: Dictionary<CameraAssignment> = {};
    cameraAssignments.forEach((ca) => {
      cameraAssignmentsByCamera[ca.CameraId] = ca;
    });

    const hydratedCameras: HydratedCamera[] = [];
    cameras.forEach((c) => {
      const vehicleAssignment = (cameraAssignmentsByCamera[c.Id]) ?
        vehicleEntities[cameraAssignmentsByCamera[c.Id].VehicleId] : null;
      const hc: HydratedCamera = {
        ...c,
        vehicleAssignment
      };
      hydratedCameras.push(hc);
    });
    return hydratedCameras;
  }
);

export const selectHydratedCameraAssignments = createSelector(
  CameraSelectors.selectCameraEntities,
  VehicleSelectors.selectVehicleEntities,
  selectAllCameraAssignments,
  UserSelectors.selectUserEntities,
  (
    cameraEntities,
    vehicleEntities,
    cameraAssignments,
    userEntities
  ) => {
    const hydratedCameraAssignments: HydratedCameraAssignment[] = [];
    cameraAssignments.forEach((ca) => {
      const hca: HydratedCameraAssignment = {
        ...ca,
        camera: cameraEntities[ca.CameraId],
        vehicle: vehicleEntities[ca.VehicleId],
        assignedBy: userEntities[ca.AssignedById]
      };
      hydratedCameraAssignments.push(hca);
    });
    return hydratedCameraAssignments;
  }
);

export const selectVehiclesWithoutCameraAssignment = createSelector(
  CameraSelectors.selectCameraEntities,
  VehicleSelectors.selectAllVehicles,
  selectAllCameraAssignments,
  (
    cameraEntities,
    vehicles,
    cameraAssignments
  ) => {
    const vehiclesWithoutCamera: Vehicle[] = [];
    const vehiclesWithCameraAssignments: Map<number, boolean> = new Map();
    cameraAssignments.forEach((ca) => {
      vehiclesWithCameraAssignments[ca.VehicleId] = true;
    });
    vehicles.forEach((v) => {
      if (!vehiclesWithCameraAssignments[v.Id]) {
        vehiclesWithoutCamera.push(v);
      }
    });
    return vehiclesWithoutCamera;
  }
);

/**
 * TODO: selectVehiclesWithoutCamera and selectUnassignedCameras are similar enough
 * TODO: consider https://www.npmjs.com/package/ngrx-entity-relationship ?
 */
export const selectUnassignedCameras = createSelector(
  CameraSelectors.selectAllCameras,
  VehicleSelectors.selectVehicleEntities,
  selectAllCameraAssignments,
  (
    cameras,
    vehicleEntities,
    cameraAssignments
  ) => {
    const unassignedCameras: Camera[] = [];
    const assignedCameras: Map<number, boolean> = new Map();
    cameraAssignments.forEach((ca) => {
      assignedCameras[ca.CameraId] = true;
    });
    cameras.forEach((c) => {
      if (!assignedCameras[c.Id]) {
        unassignedCameras.push(c);
      }
    });
    return unassignedCameras;
  }
);

export const selectCamerasByVehicle = createSelector(
  CameraAssignmentSelectors.selectAllCameraAssignments,
  CameraSelectors.selectCameraEntities,
  (cameraAssignments, cameraEntities) => {
    const camerasByVehicle: Dictionary<Camera[]> = {};
    cameraAssignments.forEach((ca) => {
      if (!camerasByVehicle[ca.VehicleId]) camerasByVehicle[ca.VehicleId] = [];
      camerasByVehicle[ca.VehicleId].push(cameraEntities[ca.CameraId]);
    });
    return camerasByVehicle;
  }
);
