import { TestBed } from '@angular/core/testing';

import { CameraFacade } from './camera.facade';

describe('Camera.Facade.TsService', () => {
  let service: CameraFacade;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CameraFacade);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
