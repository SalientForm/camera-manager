import { Camera, CameraLocations } from './camera.model';

export const camera_seedData:Array<Camera> = [
  {Id: 1, StockId: '001', DeviceNo: 'super-4-8-15-16-23-42', Location: CameraLocations.INSTALLED_IN_VEHICLE},
  {Id: 2, StockId: '002', DeviceNo: 'mystery-cam-3000', Location: CameraLocations.WAREHOUSE},
  {Id: 3, StockId: '003', DeviceNo: 'buck-shot-2500', Location: CameraLocations.INSTALLED_IN_VEHICLE},
  {Id: 4, StockId: '004', DeviceNo: 'camera-001', Location: CameraLocations.INSTALLED_IN_VEHICLE},
  {Id: 5, StockId: '005', DeviceNo: 'video-maker-9000', Location: CameraLocations.INSTALLED_IN_VEHICLE},
  {Id: 6, StockId: '006', DeviceNo: 'fast-picture-sx', Location: CameraLocations.INSTALLED_IN_VEHICLE},
];

