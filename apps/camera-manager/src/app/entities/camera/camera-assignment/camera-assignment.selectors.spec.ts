import * as fromCameraAssignment from './camera-assignment.reducer';
import { selectCameraAssignmentState } from './camera-assignment.selectors';

describe('CameraAssignment Selectors', () => {
  it('should select the feature state', () => {
    const result = selectCameraAssignmentState({
      [fromCameraAssignment.cameraAssignmentFeatureKey]: {}
    });

    expect(result).toEqual({});
  });
});
