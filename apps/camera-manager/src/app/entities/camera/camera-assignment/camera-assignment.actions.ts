import { createAction, props } from '@ngrx/store';
import { Update } from '@ngrx/entity';
import { CameraAssignment } from './camera-assignment.model';

export const loadCameraAssignments = createAction(
  '[CameraAssignment/API] Load CameraAssignments',
  props<{ cameraAssignments: CameraAssignment[] }>()
);

export const addCameraAssignment = createAction(
  '[CameraAssignment/API] Add Assignment',
  props<{ cameraAssignment: CameraAssignment }>()
);

export const upsertCameraAssignment = createAction(
  '[CameraAssignment/API] Upsert Assignment',
  props<{ cameraAssignment: CameraAssignment }>()
);

export const addCameraAssignments = createAction(
  '[CameraAssignment/API] Add CameraAssignments',
  props<{ cameraAssignments: CameraAssignment[] }>()
);

export const upsertCameraAssignments = createAction(
  '[CameraAssignment/API] Upsert CameraAssignments',
  props<{ cameraAssignments: CameraAssignment[] }>()
);

export const updateCameraAssignment = createAction(
  '[CameraAssignment/API] Update Assignment',
  props<{ cameraAssignment: Update<CameraAssignment> }>()
);

export const updateCameraAssignments = createAction(
  '[CameraAssignment/API] Update CameraAssignments',
  props<{ cameraAssignments: Update<CameraAssignment>[] }>()
);

export const deleteCameraAssignment = createAction(
  '[CameraAssignment/API] Delete Assignment',
  props<{ id: number }>()
);

export const deleteCameraAssignments = createAction(
  '[CameraAssignment/API] Delete CameraAssignments',
  props<{ ids: number[] }>()
);

export const clearCameraAssignments = createAction(
  '[CameraAssignment/API] Clear CameraAssignments'
);
