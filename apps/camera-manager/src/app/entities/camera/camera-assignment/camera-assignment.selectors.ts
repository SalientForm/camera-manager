import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromCameraAssignment from './camera-assignment.reducer';
import { CameraAssignment } from './camera-assignment.model';
import { Dictionary } from '@ngrx/entity';

export const selectCameraAssignmentState = createFeatureSelector<fromCameraAssignment.State>(
  fromCameraAssignment.cameraAssignmentFeatureKey
);

const { selectAll, selectEntities } = fromCameraAssignment.adapter.getSelectors();

export const selectAllCameraAssignments = createSelector(selectCameraAssignmentState, (state: fromCameraAssignment.State) =>
  selectAll(state)
);

export const selectCameraAssignmentEntities = createSelector(
  selectCameraAssignmentState,
  (state: fromCameraAssignment.State) => selectEntities(state)
);


export const selectCameraAssignmentsByCamera = createSelector(
  selectAllCameraAssignments,
  (cameraAssignments) => {
    const cameraAssignmentsByCamera:Dictionary<CameraAssignment> = {};
    cameraAssignments.forEach((ca)=>{
      cameraAssignmentsByCamera[ca.CameraId] = ca;
    });
    return cameraAssignmentsByCamera;
  }
);



