import { CameraAssignment } from './camera-assignment.model';

export const cameraAssignment_seedData:Array<CameraAssignment> = [
  {
    Id: 1,
    CameraId: 1,
    VehicleId: 1,
    DateCreated: 1597077009380,
    Deleted: false,
    AssignedById: 1
  },
  {
    Id: 2,
    CameraId: 2,
    VehicleId: 2,
    DateCreated: 1597077009380,
    Deleted: false,
    AssignedById: 2
  },
  // {
  //   Id: 3,
  //   CameraId: 3,
  //   VehicleId: 2,
  //   DateCreated: 1597077009380,
  //   Deleted: false,
  //   AssignedById: 3
  // },
];
