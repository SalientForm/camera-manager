import { createReducer, on } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';
import { CameraAssignment } from './camera-assignment.model';
import * as CameraAssignmentActions from './camera-assignment.actions';

export const cameraAssignmentFeatureKey = 'cameraAssignments';

export interface State extends EntityState<CameraAssignment> {
  // additional entities state properties
}

export const adapter: EntityAdapter<CameraAssignment> = createEntityAdapter<CameraAssignment>({
  selectId: (ca => ca.Id)
});

export const initialState: State = adapter.getInitialState({
  // additional entity state properties
});


export const reducer = createReducer(
  initialState,
  on(CameraAssignmentActions.addCameraAssignment,
    (state, action) => adapter.addOne(action.cameraAssignment, state)
  ),
  on(CameraAssignmentActions.upsertCameraAssignment,
    (state, action) => adapter.upsertOne(action.cameraAssignment, state)
  ),
  on(CameraAssignmentActions.addCameraAssignments,
    (state, action) => adapter.addMany(action.cameraAssignments, state)
  ),
  on(CameraAssignmentActions.upsertCameraAssignments,
    (state, action) => adapter.upsertMany(action.cameraAssignments, state)
  ),
  on(CameraAssignmentActions.updateCameraAssignment,
    (state, action) => adapter.updateOne(action.cameraAssignment, state)
  ),
  on(CameraAssignmentActions.updateCameraAssignments,
    (state, action) => adapter.updateMany(action.cameraAssignments, state)
  ),
  on(CameraAssignmentActions.deleteCameraAssignment,
    // (state, action) => adapter.removeOne(action.id, state)
    (state, action) =>
      adapter.updateOne({ id: action.id, changes: { Deleted: true }}, state)
  ),
  on(CameraAssignmentActions.deleteCameraAssignments,
    (state, action) => adapter.removeMany(action.ids, state)
  ),
  on(CameraAssignmentActions.loadCameraAssignments,
    (state, action) => adapter.setAll(action.cameraAssignments, state)
  ),
  on(CameraAssignmentActions.clearCameraAssignments,
    state => adapter.removeAll(state)
  )
);


export const {
  selectIds,
  selectEntities,
  selectAll,
  selectTotal
} = adapter.getSelectors();
