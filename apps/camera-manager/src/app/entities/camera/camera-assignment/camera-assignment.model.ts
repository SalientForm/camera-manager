import { Vehicle } from '../../vehicle/vehicle.model';
import { Camera } from '../camera.model';
import { User } from '../../user/user.model';

/**
 * Partial is defined as minimum set of attributes to create a CameraAssignment
 */
export interface CameraAssignmentPartial {
  VehicleId: number;
  CameraId: number;
  AssignedById: number;
}

export interface CameraAssignment extends CameraAssignmentPartial {
  Id: number;
  DateCreated: number;
  Deleted: boolean;
}

export interface HydratedCameraAssignment extends CameraAssignment {
  camera: Camera;
  vehicle: Vehicle;
  assignedBy: User;
}

export const CameraAssignmentFactory = (data: CameraAssignmentPartial): CameraAssignment => {
  const cameraAssignment: CameraAssignment = {
    Id: new Date().getTime(), // Note: this is a bit of a hack, client ids would either need to be unique for client, or received upon save to server
    VehicleId: data.VehicleId,
    CameraId: data.CameraId,
    DateCreated: new Date().getTime(),
    Deleted: false,
    AssignedById: data.AssignedById
  };
  return cameraAssignment;
};
