import { Vehicle } from '../vehicle/vehicle.model';
import { CameraAssignment } from './camera-assignment/camera-assignment.model';

export interface Camera {
  Id: number;
  DeviceNo: string;
  StockId: string;
  Location: string;
}

export interface HydratedCamera extends Camera {
  vehicleAssignment: Vehicle;
}

export const CameraStatuses = {
  ASSIGNED: 'assigned', // assigned but camera is not installed
  UNASSIGNED: 'unassigned' // unassigned
};

export const CameraLocations = {
  WAREHOUSE: 'warehouse', // assigned but camera is not installed
  INSTALLED_IN_VEHICLE: 'installed-in-vehicle' // unassigned
};
