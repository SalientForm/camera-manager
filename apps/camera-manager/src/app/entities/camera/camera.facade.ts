import { Injectable } from '@angular/core';
import * as fromCameras from './camera.reducer';
import { Action, select, Store } from '@ngrx/store';
import * as CameraSelectors from './camera.selectors';
import * as CameraDomainSelectors from './camera.domain-selectors';

@Injectable({
  providedIn: 'root'
})
export class CameraFacade {

  constructor(
    private store: Store<fromCameras.State>
  ) {
  }

  selectAll$ = this.store.pipe(select(CameraSelectors.selectAllCameras));
  selectTest$ = this.store.pipe(select(CameraSelectors.selectCameraState));
  selectEntities$ = this.store.pipe(select(CameraSelectors.selectCameraEntities));
  selectHydratedCameraAssignments$ = this.store.pipe(select(CameraDomainSelectors.selectHydratedCameraAssignments));
  selectHydratedCameras$ = this.store.pipe(select(CameraDomainSelectors.selectHydratedCameras));
  selectVehiclesWithoutCameraAssignment$ = this.store.pipe(select(CameraDomainSelectors.selectVehiclesWithoutCameraAssignment));
  selectAllUnassignedCameras$ = this.store.pipe(select(CameraDomainSelectors.selectUnassignedCameras));

  dispatch(action: Action) {
    this.store.dispatch(action);
  }
}
