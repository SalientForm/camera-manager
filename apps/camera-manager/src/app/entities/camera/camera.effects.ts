import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { switchMap } from 'rxjs/operators';
import * as CameraActions from './camera.actions';
import * as CameraAssignmentActions from '../camera/camera-assignment/camera-assignment.actions';
import * as VehicleActions from '../vehicle/vehicle.actions';
import * as UserActions from '../user/user.actions';
import { camera_seedData } from './camera.mock';
import { cameraAssignment_seedData } from '../camera/camera-assignment/camera-assignment.mock';
import { vehicle_seedData } from '../vehicle/vehicle.mock';
import { user_seedData } from '../user/user.mock';

@Injectable()
export class CameraEffects {

  /**
   * Loads all seeded mock data here in one place. It is usually not preferred to automatically load data from the
   * server in this manner, but this works for this exercise.
   */
  initCamera$ = createEffect(() =>
    this.actions$.pipe(
      ofType('@ngrx/effects/init'),
      switchMap((action) => [
        UserActions.loadUsers({ users: user_seedData }),
        UserActions.setLocalUser({ id: 1 }),
        CameraActions.loadCameras({ cameras: camera_seedData }),
        VehicleActions.loadVehicles({ vehicles: vehicle_seedData }),
        CameraAssignmentActions.loadCameraAssignments({ cameraAssignments: cameraAssignment_seedData }),
      ])
    )
  );

  constructor(private actions$: Actions) {
  }

}
