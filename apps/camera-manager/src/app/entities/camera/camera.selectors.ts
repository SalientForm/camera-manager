import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromCameras from './camera.reducer';

export const selectCameraState = createFeatureSelector<fromCameras.State>(fromCameras.cameraFeatureKey);

const { selectAll, selectEntities } = fromCameras.adapter.getSelectors();

export const selectAllCameras = createSelector(
  selectCameraState,
  (state: fromCameras.State) => selectAll(state)
);

export const selectCameraEntities = createSelector(
  selectCameraState,
  (state: fromCameras.State) => selectEntities(state)
);


