import * as fromCamera from './camera.reducer';
import { selectCameraState } from './camera.selectors';

describe('Camera Selectors', () => {
  it('should select the feature state', () => {
    const result = selectCameraState({
      [fromCamera.cameraFeatureKey]: {}
    });

    expect(result).toEqual({});
  });
});
