import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CameraAssignmentsComponent } from '../views/camera-assignments/camera-assignments.component';
import { CamerasComponent } from '../views/cameras/cameras.component';
import { CreateCameraAssignmentComponent } from '../views/create-camera-assignment/create-camera-assignment.component';
import { SummaryComponent } from '../views/summary/summary.component';
import { VehiclesComponent } from '../views/vehicles/vehicles.component';
import { CoreComponent } from './core.component';
import { StandardLayoutModule } from '../views/layouts/standard/standard-layout.module';
import { StoreModule } from '@ngrx/store';
import * as fromCamera from '../entities/camera/camera.reducer';
import * as fromCameraAssignment from '../entities/camera/camera-assignment/camera-assignment.reducer';
import * as fromLayout from '../views/layouts/standard/state/layout.reducer';
import * as fromUser from '../entities/user/user.reducer';
import * as fromVehicle from '../entities/vehicle/vehicle.reducer';
import { EffectsModule } from '@ngrx/effects';
import { LayoutEffects } from '../views/layouts/standard/state/layout.effects';
import { CameraEffects } from '../entities/camera/camera.effects';
import { environment } from '../../environments/environment';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import { ConfirmDialogComponent } from '../shared/confirm-dialog/confirm-dialog.component';

const ngrxRootConfig = {
  metaReducers: !environment.production ? [] : [],
  runtimeChecks: {
    strictActionImmutability: true,
    strictStateImmutability: true
  }
};

@NgModule({
  declarations: [
    CameraAssignmentsComponent,
    CamerasComponent,
    CoreComponent,
    CreateCameraAssignmentComponent,
    SummaryComponent,
    VehiclesComponent,
  ],
  imports: [
    CommonModule,
    EffectsModule.forFeature([LayoutEffects]),
    EffectsModule.forRoot([CameraEffects]),
    ReactiveFormsModule,
    StandardLayoutModule,
    StoreModule.forFeature(fromCamera.cameraFeatureKey, fromCamera.reducer),
    StoreModule.forFeature(fromCameraAssignment.cameraAssignmentFeatureKey, fromCameraAssignment.reducer),
    StoreModule.forFeature(fromLayout.layoutFeatureKey, fromLayout.reducer),
    StoreModule.forFeature(fromUser.userFeatureKey, fromUser.reducer),
    StoreModule.forFeature(fromVehicle.vehicleFeatureKey, fromVehicle.reducer),
    StoreModule.forRoot({}, ngrxRootConfig),
    SharedModule
  ],
  exports: [
    CoreComponent
  ],
  entryComponents: [
    CreateCameraAssignmentComponent
  ]
})
export class CoreModule { }
