import { SummaryComponent } from './views/summary/summary.component';
import { RouterModule } from '@angular/router';
import { CamerasComponent } from './views/cameras/cameras.component';
import { CameraAssignmentsComponent } from './views/camera-assignments/camera-assignments.component';
import { NgModule } from '@angular/core';
import { VehiclesComponent } from './views/vehicles/vehicles.component';

const routes = [
  { path: '', redirectTo: '/assignments', pathMatch: 'full' },
  { path: 'summary', component: SummaryComponent },
  { path: 'cameras', component: CamerasComponent },
  { path: 'assignments', component: CameraAssignmentsComponent },
  { path: 'vehicles', component: VehiclesComponent },
  { path: '**', redirectTo:'/', }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: []
})

export class AppRoutingModule {

}
